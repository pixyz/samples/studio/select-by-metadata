# Select By Metadata

The export-to-unity plugin is a Pixyz Studio [module](https://www.pixyz-software.com/documentations/html/2020.1/studio/Plugins.html). Select all occurrences which a metadata property value matches the given regular expression (ECMAScript). The plugin is accessible either from the **Select** menu or from the **Plugins** menu.

## Required Software

![](doc/puce-studio-64x64.png)|
:-:|
Studio|

## Set up

* Clone or download select-by-metadata folder at `C:/ProgramData/PiXYZStudio/plugins`
